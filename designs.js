function makeGrid() 
{
	var table=document.getElementById('pixelCanvas');
	table.innerHTML="";
	var rows=document.getElementById('inputHeight').value;
	var cols=document.getElementById('inputWidth').value;
	var tabledata='';
	for (var i=0;i<rows;i++)
	{
		tabledata+='<tr>';
		for (var x=0;x<cols;x++)
		{
			tabledata+='<td></td>';
		}	
		tabledata+='</tr>';
	}
	table.innerHTML=tabledata;
	AddEventForTD();
}
function AddEventForTD ()
{
	var tds=document.getElementsByTagName('td');
	for (var i=0;i<tds.length;i++)
	{
		tds[i].addEventListener ("click" , function(event) {
			var currenttd=event.target;
			currenttd.style.backgroundColor=document.getElementById('colorPicker').value;

		})
	}
}

document.addEventListener('DOMContentLoaded', function () {
	AddEventForTD();

} )
